//
//  ViewController.m
//  RSAEncryption
//
//  Created by Austin Drummond on 3/2/14.
//  Copyright (c) 2014 Austin Drummond. All rights reserved.
//

#import "ViewController.h"
#import <Security/Security.h>

@interface ViewController ()

@property (strong,nonatomic) IBOutlet UITextView *encrypted;
@property (strong,nonatomic) IBOutlet UITextField *unencrypted;

@end

@implementation ViewController
@synthesize encrypted,unencrypted;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)closeKeyboard:(id)sender{
    [self.view endEditing:YES];
}

-(IBAction)pressed:(id)sender {
    
    
    encrypted.text = [self encrypt:unencrypted.text];
    [self.view endEditing:YES];
}

-(NSString*)encrypt:(NSString*)string {
    
    /* ---- 2 ---- Get it from an NSString...*/
    NSData *inputData = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    const void *bytes = [inputData bytes];
    /* in case inputData is released before we're done... */
    int length = [inputData length];
    uint8_t *plainText = malloc(length);
    memcpy(plainText, bytes, length);
    /* ---- 2 ---- */
    
//    NSString* certPath = [[NSBundle mainBundle] pathForResource:@"cert" ofType:@"cer"];
    NSString* certPath = [[NSBundle mainBundle] pathForResource:@"public_key" ofType:@"der"];
    NSData* certData = [NSData dataWithContentsOfFile:certPath];
    
    SecCertificateRef cert = NULL;
    if( [certData length] ) {
        cert = SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certData);
        if( cert != NULL ) {
            CFStringRef certSummary = SecCertificateCopySubjectSummary(cert);
            //NSString* summaryString = [[NSString alloc] initWithString:(__bridge NSString*)certSummary];
            //NSLog(@"CERT SUMMARY: %@", summaryString);
            CFRelease(certSummary);
        } else {
            //NSLog(@" *** ERROR *** trying to create the SSL certificate from data, but failed");
        }
    }
    // SecCertificateRef cert = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)certData);
    SecPolicyRef policy = SecPolicyCreateBasicX509();
    SecTrustRef trust;
    OSStatus status = SecTrustCreateWithCertificates(cert, policy, &trust);
    
    /* You can ignore the SecTrustResultType, but you have to run SecTrustEvaluate
     * before you can get the public key */
    SecTrustResultType trustResult;
    if (status == noErr) {
        status = SecTrustEvaluate(trust, &trustResult);
    }
    
    /* Now grab the public key from the cert */
    SecKeyRef publicKey = SecTrustCopyPublicKey(trust);
    
    /* allocate a buffer to hold the cipher text */
    size_t cipherBufferSize;
    uint8_t *cipherBuffer;
    cipherBufferSize = SecKeyGetBlockSize(publicKey);
    cipherBuffer = malloc(cipherBufferSize);
    
    /* encrypt!! */
    SecKeyEncrypt(publicKey, kSecPaddingPKCS1, plainText, length, cipherBuffer, &cipherBufferSize);
    
    
    cipherBufferSize = SecKeyGetBlockSize(publicKey);
    cipherBuffer = malloc(cipherBufferSize);
    
    /* encrypt!! */
    SecKeyEncrypt(publicKey, kSecPaddingPKCS1, plainText, length, cipherBuffer, &cipherBufferSize);
    
    /* Do something with your cipher text...like convert it to an NSData object,
     base64 encode it and send it to your server.
     */
    
    NSData *d = [NSData dataWithBytes:cipherBuffer length:cipherBufferSize];
    NSString *base = [self base64forData:d];
    
    /* Free the Security Framework Five! */
    CFRelease(cert);
    CFRelease(policy);
    CFRelease(trust);
    CFRelease(publicKey);
    free(cipherBuffer);
    
    /* And this guy if you used #2 above (got your plain text from an NSString) */
    free(plainText);

    return base;
    
    
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


//
//
//
//-(void)encryptData:(NSString *)kind {
//    
//    /* ---- 2 ---- Get it from an NSString...*/
//    NSData *inputData = [radioKit.currTitle dataUsingEncoding:NSUTF8StringEncoding];
//    
//    const void *bytes = [inputData bytes];
//    /* in case inputData is released before we're done... */
//    int length = [inputData length];
//    uint8_t *plainText = malloc(length);
//    memcpy(plainText, bytes, length);
//    /* ---- 2 ---- */
//    
//    NSString* certPath = [[NSBundle mainBundle] pathForResource:@"cert" ofType:@"cer"];
//    NSData* certData = [NSData dataWithContentsOfFile:certPath];
//    
//    SecCertificateRef cert = NULL;
//    if( [certData length] ) {
//        cert = SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certData);
//        if( cert != NULL ) {
//            CFStringRef certSummary = SecCertificateCopySubjectSummary(cert);
//            //NSString* summaryString = [[NSString alloc] initWithString:(__bridge NSString*)certSummary];
//            //NSLog(@"CERT SUMMARY: %@", summaryString);
//            CFRelease(certSummary);
//        } else {
//            //NSLog(@" *** ERROR *** trying to create the SSL certificate from data, but failed");
//        }
//    }
//    // SecCertificateRef cert = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)certData);
//    SecPolicyRef policy = SecPolicyCreateBasicX509();
//    SecTrustRef trust;
//    OSStatus status = SecTrustCreateWithCertificates(cert, policy, &trust);
//    
//    /* You can ignore the SecTrustResultType, but you have to run SecTrustEvaluate
//     * before you can get the public key */
//    SecTrustResultType trustResult;
//    if (status == noErr) {
//        status = SecTrustEvaluate(trust, &trustResult);
//    }
//    
//    /* Now grab the public key from the cert */
//    SecKeyRef publicKey = SecTrustCopyPublicKey(trust);
//    
//    /* allocate a buffer to hold the cipher text */
//    size_t cipherBufferSize;
//    uint8_t *cipherBuffer;
//    cipherBufferSize = SecKeyGetBlockSize(publicKey);
//    cipherBuffer = malloc(cipherBufferSize);
//    
//    /* encrypt!! */
//    SecKeyEncrypt(publicKey, kSecPaddingPKCS1, plainText, length, cipherBuffer, &cipherBufferSize);
//    
//    
//    cipherBufferSize = SecKeyGetBlockSize(publicKey);
//    cipherBuffer = malloc(cipherBufferSize);
//    
//    /* encrypt!! */
//    SecKeyEncrypt(publicKey, kSecPaddingPKCS1, plainText, length, cipherBuffer, &cipherBufferSize);
//    
//    /* Do something with your cipher text...like convert it to an NSData object,
//     base64 encode it and send it to your server.
//     */
//    
//    NSData *d = [NSData dataWithBytes:cipherBuffer length:cipherBufferSize];
//    NSString *base = [self base64forData:d];
//    
//    //NSLog(@"%@",radioKit.currTitle);
//    //NSLog(@"%@", base);
//    
//
//    
//    
//    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    
//    if (internetStatus != NotReachable) {
//        NSString *dbURL = @"";
//        
//        NSError *error;
//        
//        // Sendy stuff here - AFNetworking libraries are a good choice
//        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Christmas"] == NO)
//            dbURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:DB_URL] encoding:NSUTF8StringEncoding error:&error];
//        else
//            dbURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:XMAS_DB_URL] encoding:NSUTF8StringEncoding error:&error];
//        
//        
//        NSURL *url;
//        if ([kind isEqualToString:@"Like"])
//            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/up",dbURL]];
//        else
//            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/dwn",dbURL]];
//        
//        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
//        NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
//                                                                path:@""
//                                                          parameters:@{@"vote":base, @"data":radioKit.currTitle}];
//        
//        NSLog(@"%@",url);
//        
//        
//        NSHTTPURLResponse* response = nil;
//        
//        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        
//        if (error)
//            if (debug)
//                NSLog(@"%@",data);
//        
//        if (debug)
//            NSLog(@"Response Code: %d", [response statusCode]);
//        
//        if ([response statusCode] >= 200 && [response statusCode] < 300) {
//            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark.png"]];
//            // Set custom view mode
//            HUD.mode = MBProgressHUDModeCustomView;
//            HUD.labelText = @"Vote Accepted";
//            [HUD hide:YES afterDelay:2];
//        } else {
//            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Xmark.png"]];
//            // Set custom view mode
//            HUD.mode = MBProgressHUDModeCustomView;
//            HUD.labelText = @"Vote Failed";
//            [HUD hide:YES afterDelay:2];
//        }
//    
//    
//    }
//}

@end
